﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 30.01.2020
 * Time: 8:51
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace test_arrays2
{
	class Program
	{
		public static void CreateArray()
		{
			int[,] myArray = new int[9, 9];
			Console.WriteLine(myArray.Length);
			int center = 9 / 2;
			for(int i=0;i<9;++i)
			{
				myArray[i, i] =myArray[i,8-i]=1;
				myArray[center, i] = myArray[i,center] = 2;


			}

			for (int i = 0; i < 9; ++i)
			{
				for (int j = 0; j < 9; ++j)
				{
					Console.Write(myArray[i, j] + " ");
				}
				Console.Write("\n");
			}
			Console.ReadKey();

		}
		//Fraction(string str) "2/5"  "2/5/9"
		//{
		//   string[] s=str.Split("/");
		//   if(s.Len!=2)
		//       throw new 
		//   a=int.Parse(s[0]);
		//   b=int.Parse(s[1]);
		//}
		public static void Main(string[] args)
		{
			//CreateArray();
			//return;
			
			Console.ForegroundColor=ConsoleColor.Green;
			double [,] field;
			int [,] fieldset=new int[8,8];
			
			field=new double[3,3]{{1,2,3},{4,5,6},{7,8,9}};
			
			Console.WriteLine("\nField:");
			foreach(double element in field)
			{
				Console.Write(element+" ");
			}
			
			Console.WriteLine("\nField:");
			for(int i=0;i<3;++i,Console.Write("\n"))
				for(int j=0;j<3;++j)
			    {
				    Console.Write(field[i,j]+" ");
			    }

			int[][] dataset;
			dataset=new int[4][];
			for (int i = 0; i < 4; ++i)
			{
				dataset[i] = new int[2 * i + 2];
			}
//			foreach(var rec in dataset)
//				Console.WriteLine(rec);
			Console.WriteLine("\nDataset:");
			foreach(var rec in dataset)
			{
				foreach(int element in rec)
					Console.Write(element+" ");
				Console.WriteLine();
			}
			Console.WriteLine("\nDataset:");
			for(int i=0;i<4;++i,Console.Write("\n"))
				for(int j=0;j<dataset[i].Length;++j)
			    {
				Console.Write(dataset[i][j]+" ");
			    }
			
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
			
		}
		public static void DumpArray(int [,] arr)
		{
			for (int i = 0; i < arr.Length; ++i)
			{
				for (int j = 0; j < 3; ++j)
				{
					Console.Write(arr[i, j] + " ");
				}
				Console.Write("\n");
			}
		}
	}
}