﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 30.01.2020
 * Time: 9:27
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace test_array_common
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.ForegroundColor=ConsoleColor.Green;
			double[] badArray=new double[8]{2.5,6.1,7.2,9.9,11.7,15.5,21.0,88.0};
			int[] index=new int[8]{7,11,24,45,24,17,66,4};
			dumpArray(index,"Index");
			dumpArray(badArray,"badArray");
			string[] stranger=Array.ConvertAll(badArray,expr=>expr.ToString());
			int[] walker=Array.ConvertAll(badArray,reduce);
			Console.WriteLine();
			Console.WriteLine(string.Join(",",stranger));
			dumpArray(walker,"walker");
			Console.Write("\nPress any key to continue . . . ");
			Console.ReadKey(true);
		}
		public static void dumpArray(int[] array,string name="Array")
		{
			Console.WriteLine("\n{0}:",name);
			foreach(var element in array)
				Console.Write(element+" "); //+' '
		}
		public static void dumpArray(double[] array,string name="Array")
		{
			Console.WriteLine("\n{0}:",name);
			foreach(var element in array)
				Console.Write(element+" "); //+' '
		}
		
		public static int reduce(double x)
		{
			return (int) x;
		}
	}
}
