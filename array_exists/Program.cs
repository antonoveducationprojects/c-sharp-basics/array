﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 30.01.2020
 * Time: 9:27
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace test_array_common
{
	class Program
	{
		static int value=45;
		public static void Main(string[] args)
		{
			Console.ForegroundColor=ConsoleColor.Green;
			int[] index=new int[8]{7,11,24,45,24,17,66,4};
			dumpArray(index,"Index");
			
			Predicate<int> predicat=GetElements;
			if(Array.Exists(index,predicat))
				Console.WriteLine("\nValue {0} exist in array",value);
			else
				Console.WriteLine("\nValue {0} not exist in array",value);
			
			Console.Write("\nPress any key to continue . . . ");
			Console.ReadKey(true);
		}
		public static void dumpArray(int[] array,string name="Array")
		{
			Console.WriteLine("\n{0}:",name);
			foreach(int element in array)
				Console.Write(element+" "); //+' '
		}
		public static bool GetElements(int val)
		{
			return value==val;
		}
	}
}
