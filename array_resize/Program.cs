﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 30.01.2020
 * Time: 9:27
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace test_array_common
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.ForegroundColor=ConsoleColor.Green;
			int[] index=new int[8]{7,11,24,45,24,17,66,4};
			int[] clone=(int[])index.Clone();
			dumpArray(index,"Index");
			Array.Resize(ref index,  15);
			dumpArray(index,"Index");
			dumpArray(clone,"Clone");
			Console.Write("\nPress any key to continue . . . ");
			Console.ReadKey(true);
		}
		public static void dumpArray(int[] array,string name="Array")
		{
			Console.WriteLine("\n{0}:",name);
			foreach(int element in array)
				Console.Write(element+" "); //+' '
		}
	}
}
