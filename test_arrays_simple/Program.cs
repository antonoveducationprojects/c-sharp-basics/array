﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 29.01.2020
 * Time: 22:33
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace test_elementary
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.ForegroundColor=ConsoleColor.Green;
			int [] indexes=new int[8]{7,11,24,45,24,17,66,4};
			Console.WriteLine("Indexes:");
			dumpArray(indexes);
			Console.WriteLine("Array Capaticity");
			Console.WriteLine("Array Lenght       :{0}",indexes.Length);
			Console.WriteLine("Array GetLenght    :{0}",indexes.GetLength(0));
			Console.WriteLine("Array LongLenght   :{0}",indexes.LongLength);
			Console.WriteLine("Array GetLongLenght:{0}",indexes.GetLongLength(0));
			Console.WriteLine("Array HashCode     :{0}",indexes.GetHashCode());
			Console.WriteLine("Array GetLowerBound:{0}",indexes.GetLowerBound(0));
			Console.WriteLine("Array GetUpperBound:{0}",indexes.GetUpperBound(0));
			Console.WriteLine("Array GetType      :{0}",indexes.GetType());
			Console.WriteLine("Array isReadOnly   :{0}",indexes.IsReadOnly);
			Console.WriteLine("Array Rank         :{0}",indexes.Rank);
			Console.WriteLine("Array ToString     :{0}",indexes.ToString());
					
			Console.Write("\nPress any key to continue . . . ");
			Console.ReadKey(true);
		}
		public static void dumpArray(int[] array)
		{
			foreach(int element in array)
				Console.Write(element+" "); //+' '
		}
	}
}