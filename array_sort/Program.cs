﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 30.01.2020
 * Time: 9:27
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace test_array_common
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.ForegroundColor=ConsoleColor.Green;
			int[] index=new int[8] {7,11,24,45,24,17,66,4};
			//int[] ind2 = new int[] { 5, 6, 7, 8,0 };
			int[] copy=index;
			int[] clone=(int[])index.Clone();
			dumpArray(index,"Index");
			dumpArray(copy,"Copy");			
			Array.Sort(index);
			dumpArray(index,"Index");
			dumpArray(copy,"Copy");
			dumpArray(clone,"Clone");

			int value=24;
			int searchIndex=Array.BinarySearch(copy,value);
			if(searchIndex<0)
				Console.WriteLine("\nBinarySearch: value {0} not found",value);
			else
				Console.WriteLine("\nBinarySeaarch: value {0} placed at {1}",value,searchIndex);

			Console.Write("\nPress any key to continue . . . ");
			Console.ReadKey(true);
		}
		public static void dumpArray(int[] array,string name="Array")
		{
			Console.WriteLine("\n{0}:",name);
			foreach(int element in array)
				Console.Write(element+" "); //+' '
		}
	}
}
